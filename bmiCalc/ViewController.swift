//
//  ViewController.swift
//  bmiCalc
//
//  Created by iosdev on 15.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit

var bmiHistory:[Person] = Array()


    class ViewController: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource,
            UITextFieldDelegate {

            let alertController = UIAlertController(title: "Alert", message: "This is an alert.", preferredStyle: .alert)
            let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
            print("Numbers not allowed");
            }
    
    
    private var heightDataSource:[Int] = Array()
    private var weightDatatSource:[Int] = Array()
        
        @IBOutlet weak var nameField: UITextField!
        @IBOutlet weak var pickerView: UIPickerView!
        @IBOutlet weak var display: UILabel!
        
        
    var selectedHeight = 0
    var selectedWeight = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        for h in 140...200 {
                heightDataSource.append(h)
            }
            for w in 40...100 {
                weightDatatSource.append(w)
            }
            nameField.delegate = self as UITextFieldDelegate
            pickerView.delegate = self as UIPickerViewDelegate
            pickerView.dataSource = self as UIPickerViewDataSource
          
        }
        
         func numberOfComponents(in pickerView: UIPickerView) -> Int {
               return 2
           }
           
           
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            if (component == 0) {
                return heightDataSource.count
            }else{
                return weightDatatSource.count
            }
        }
        
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            if (component == 0) {
                return "\(heightDataSource[row]) cm"
            }else{
               return "\(weightDatatSource[row]) kg"
            }
            
        }
        
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            selectedHeight = heightDataSource[pickerView.selectedRow(inComponent: 0)]
            selectedWeight = weightDatatSource[pickerView.selectedRow(inComponent: 1)]
            
        }
    
        
        @IBAction func calculateBmi(_ sender: UIButton) {
            print("clickedddd\(selectedHeight) \(selectedWeight)" )
                       
                      //   print("clickeddd mm \(selectedHeight) \(selectedWeight)" )
                       if(Bool(truncating: selectedWeight as NSNumber) && Bool(truncating: selectedHeight as NSNumber) ) {
                           let person = Person(name: nameField.text ?? "no name", height: selectedHeight, weight: selectedWeight)
                        display.text = "lllllll"
                        display.text = "\(person.name) has \(String(person.bmiCalculate())) of BMI"
                        print("gggh",person.bmiCalculate())
                           bmiHistory.append(person)
                       }
            
        }
        @IBAction func FieldNotEmpty(_ sender: Any) {
            if (nameField.text?.isEmpty ?? true) {
                let alert = UIAlertController(title: "Alert", message: "TextField must not be empty", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
        }
        
       @IBAction func changeView(_ sender: UIButton) {
        performSegue(withIdentifier: "segue", sender: self)
        }
        
        
        
        
        func textFieldDidBeginEditing(_ textField: UITextField) {
            print("TextField did begin editing method called")
        }
        func textFieldDidEndEditing(_ textField: UITextField) {
            print("TextField did end editing method called\(textField.text!)")
        }
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            print("TextField should begin editing method called")
            return true;
        }
        func textFieldShouldClear(_ textField: UITextField) -> Bool {
            print("TextField should clear method called")
            return true;
        }
        func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
            print("TextField should end editing method called")
            return true;
        }
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            print("While entering the characters this method gets called")
            
            guard CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSUVWXYZabcdefghijklmnopqrstuvwxyz").isSuperset(of: CharacterSet(charactersIn: string)) else {
                
                alertController.addAction(action2)
                self.present(alertController, animated: true, completion: nil)

                   return false
               }
               return true
        }
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            print("TextField should return method called")
            textField.resignFirstResponder();
            return true;
        }

        
        // Do any additional setup after loading the view.
    }


