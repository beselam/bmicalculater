//
//  HistoryViewController.swift
//  bmiCalc
//
//  Created by iosdev on 15.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

//
//  HistoryViewController.swift
//  BMI Calculator
//
//  Created by iosdev on 30.3.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController, UITableViewDelegate , UITableViewDataSource{
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bmiHistory.count
      }
    
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        cell.textLabel?.text = "\(bmiHistory[indexPath.row].height), \(bmiHistory[indexPath.row].weight), \(bmiHistory[indexPath.row].BMI)"
            return cell
      }}
      
     
