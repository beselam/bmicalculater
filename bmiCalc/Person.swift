//
//  File.swift
//  bmiCalc
//
//  Created by iosdev on 15.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import Foundation
//
//  Person.swift
//  BMI Calculator
//
//  Created by iosdev on 30.3.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

class Person {
    
       let name:String
       let minAge = 18
       let minHeight = 130
       let minWeight = 48
       let minProfession = 3
       var BMI = 0.0
    
    private(set) var height:Int
    private(set) var weight:Int
    private(set) var age = 80
    private(set) var profession = 5
    
       init(name: String, height: Int, weight: Int) {
               self.name = name
               self.height = height
               self.weight = weight
           }
         
         
        func setAge (_ age:Int){
            if(age >= minAge){
                self.age = age
            }else{
                self.age = minAge
            }
        }
        
        func setProfession (_ profession:Int){
            if(profession >= minProfession){
                self.profession = profession
            }else{
                self.profession = minProfession
            }
        }
        
        func setHeight(_ height:Int){
            if(height >= minHeight){
                self.height = height
            }else{
                self.height = minHeight
            }
        }
        
        func setWeight(_ weight:Int){
            if(weight >= minWeight){
                self.weight = weight
            }else{
                self.weight = minWeight
            }
        }
    
    func bmiCalculate() -> Double {
        let roudedHeight:Double = Double(height) / 100
        let result = Double(weight) / (roudedHeight * roudedHeight)
        BMI = Double(round(10*result)/10)
        return Double(round(10*result)/10)
    }
    
}

