//
//  bmiCalcUITests.swift
//  bmiCalcUITests
//
//  Created by iosdev on 15.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import XCTest
class bmiCalcUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testValidFeaturesSuccess(){
        
        let app = XCUIApplication()
        
        XCTAssertEqual(app.pickerWheels.count, 2)
        XCTAssertEqual(app.buttons.count, 2)
        XCTAssertEqual(app.textFields.count, 1)
        
        let heightPickerDoesPick = app/*@START_MENU_TOKEN@*/.pickerWheels["140 cm"]/*[[".pickers.pickerWheels[\"140 cm\"]",".pickerWheels[\"140 cm\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        XCTAssertTrue(heightPickerDoesPick.exists)
        heightPickerDoesPick.tap()
        
        let weightPickerDoesPick = app/*@START_MENU_TOKEN@*/.pickerWheels["40 kg"]/*[[".pickers.pickerWheels[\"40 kg\"]",".pickerWheels[\"40 kg\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        XCTAssertTrue(weightPickerDoesPick.exists)
        weightPickerDoesPick.tap()
        
        let historyButton = app/*@START_MENU_TOKEN@*/.staticTexts["History"]/*[[".buttons[\"History\"].staticTexts[\"History\"]",".staticTexts[\"History\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        XCTAssertTrue(historyButton.exists)
        historyButton.tap()
        
    }

    func testLaunchPerformance() {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
                XCUIApplication().launch()
            }
        }
    }
        
}
